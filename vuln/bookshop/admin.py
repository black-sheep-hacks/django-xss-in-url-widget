from django.contrib import admin
from django.utils.html import mark_safe

from .models import Author, Book


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    exclude = []
    list_display = ['first_name', 'last_name', 'personal_page']


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    exclude = []
    list_display = ['title', 'author']
