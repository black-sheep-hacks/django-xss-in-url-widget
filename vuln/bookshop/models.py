from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=128)
    author = models.ForeignKey('bookshop.Author', on_delete=models.CASCADE, related_name='books')
    pages_count = models.IntegerField(default=0)

    def __str__(self):
        return self.title


class Author(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    personal_page = models.URLField(null=True, blank=True)

    @property
    def books_count(self):
        return self.books.count()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
