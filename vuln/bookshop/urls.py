from django.urls import path

from .views import *

app_name = 'bookshop'
urlpatterns = [
    path('', BooksListView.as_view(), name='book-list'),
    path('book/<int:pk>/', BookDetailView.as_view(), name='book-detail'),
    path('author/<int:pk>/', AuthorDetailView.as_view(), name='author-detail'),
    path('author/create/', AuthorCreateView.as_view(), name='create-author'),
    path('author/create-shitty/', ShittyAuthorCreateView.as_view(), name='shitty-create-author'),
    path('book/create/', BookCreateView.as_view(), name='create-book'),
]
