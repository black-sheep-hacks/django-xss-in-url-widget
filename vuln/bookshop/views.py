from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from bookshop.forms import ShittyAuthorForm
from .models import Author, Book
from django.views.generic import CreateView, DetailView, ListView, View


__all__ = (
    'BooksListView', 'BookDetailView', 'AuthorDetailView', 'AuthorCreateView', 'BookCreateView',
    'ShittyAuthorCreateView',
)


class BooksListView(ListView):
    model = Book
    template_name = 'bookshop/books_list.html'

    def get_queryset(self):
        return self.model.objects.all().prefetch_related('author')


class BookDetailView(DetailView):
    model = Book


class AuthorDetailView(DetailView):
    model = Author


class AuthorCreateView(CreateView):
    model = Author
    fields = '__all__'
    template_name_suffix = '_create_form'
    success_url = '/'


class ShittyAuthorCreateView(View):
    form_class = ShittyAuthorForm

    def get(self, request: WSGIRequest) -> HttpResponse:
        form = self.form_class()
        return render(request, 'bookshop/author_create_form.html', {'form': form})

    def post(self, request: WSGIRequest) -> HttpResponseRedirect:
        Author.objects.create(
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            personal_page=request.POST['personal_page'],
        )
        return HttpResponseRedirect(reverse('bookshop:book-list'))


class BookCreateView(CreateView):
    model = Book
    fields = '__all__'
    template_name_suffix = '_create_form'
    success_url = '/'
