In order to run this project:

1. Clone it
2. Create virtual environment: `virtualenv venv -p python 3`
3. Activate it `source venv/bin/activate`
4. Install dependencies: `pip install -r requirements.txt`
5. Create database: `touch db.sqlite3`
6. Migrate: `python manage.py migrate`
7. Create admin account: `python manage.py createsuperuser` - you will have to pass some imaginary data here.
8. Run project: `python manage.py runserver`

